/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superKey.cipherTool;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import superkey.keychain.CipherTool;
import superkey.keychain.KeyChain;


/**
 *
 * @author matos
 */
public class CipherToolTest {
    private CipherTool cipherTool;
    private KeyChain chain;
    private static final String KEYCHAIN_MASTER_KEY = "#wisper"; // "#wisper";
    public CipherToolTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    @Test(expected = IOException.class)
    public void testDecipherWithInvalidKey() throws IOException{
        cipherTool = new CipherTool("some word");
        cipherTool.readProtectedKeychain(chain, new File("Keychain.txt"));
       
    }
    @Test
    public void testDecipherWithValidKey(){
        KeyChain tempChain = null; 
        try {
            tempChain = KeyChain.from(new File("Keychain.txt"), new CipherTool(KEYCHAIN_MASTER_KEY));
            chain = KeyChain.from(new File("Keychain.txt"), new CipherTool(KEYCHAIN_MASTER_KEY));
        } catch (IOException ex) {
            Logger.getLogger(CipherToolTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        assertEquals("failed to decipher properly the keychain",tempChain, chain);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
